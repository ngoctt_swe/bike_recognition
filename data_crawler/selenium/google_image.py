import sys
import requests
import pandas as pd
import time
import threading
import psycopg2
import pathlib
import hashlib
import logging
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine

sys.path.insert(0, '../../database_migration')
from config import config
from config import config_url


class GoogleImage:
    def __init__(self):
        self.chrome_options = Options()
        self.chrome_options.add_argument("--headless")
        self.data_path = str(pathlib.Path(__file__).resolve().parents[2]) + "/data/"
        self.url = 'https://www.google.com/imghp?hl=EN'

    def create_image_path(self, title):
        return self.data_path + hashlib.sha1(title.encode()).hexdigest() + '.jpg'

    def store_to_folder(self, string, im_url):
        try:
            image_path = self.create_image_path(string)
            image = requests.get(im_url)
            if image.status_code == 200:
                with open(image_path, 'wb') as f:
                    f.write(image.content)
                    f.close()
            print("Image {} saved at: {}".format(string, image_path))
            return True
        except Exception as e:
            print("Failed to be downloaded {}".format(string))
            return False
    
    def store_to_database(df):
        engine = create_engine(config_url())
        df.to_sql('image', con=engine, index=False, if_exists='append', chunksize=200)

    def get_images(self, queries):
        df = pd.DataFrame(columns=['brand', 'model', 'type', 'year', 'image_url'])
        for query in queries:
            driver = webdriver.Chrome('./chromedriver', options=self.chrome_options)

            driver.get(self.url)

            box = driver.find_element_by_xpath('//*[@id="sbtc"]/div/div[2]/input')
            box.send_keys(query)
            box.send_keys(Keys.ENTER)

            last_height = driver.execute_script('return document.body.scrollHeight')
            while True:
                driver.execute_script('window.scrollTo(0,document.body.scrollHeight)')
                time.sleep(2)
                new_height = driver.execute_script('return document.body.scrollHeight')
                try:
                    driver.find_element_by_xpath('//*[@id="islmp"]/div/div/div/div/div[5]/input').click()
                    time.sleep(2)
                except:
                    pass
                if new_height == last_height:
                    break
                last_height = new_height

            page_source = BeautifulSoup(driver.page_source, features="html.parser")
            images = page_source.find_all('img', class_='rg_i Q4LuWd')
            image_numbers = len(images)
            print("number of images {}".format(image_numbers))
            for i in range(1, image_numbers + 1):
                try:
                    e = driver.find_element_by_xpath('//*[@id="islrg"]/div[1]/div[' + str(i) + ']/a[1]/div[1]/img')
                    e.click()
                except:
                    print("cant click photo number" + str(i))
                    continue

                try:
                    time.sleep(1)
                    print(query)
                    image = driver.find_element_by_xpath(
                        '//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div[2]/div[1]/a/img')
                    string = query + " " + str(i)
                    im_url = image.get_attribute("src")
                    if not (self.store_to_folder(string, im_url)):
                        e.screenshot(self.create_image_path(string))
                        print("using screenshot instead")
                    x = query.split(" + ")
                    data = {'brand': x[0], 'model': x[1], 'type': x[2], 'year': x[3]}
                    data['image_url'] = 'data/' + hashlib.sha1(string.encode()).hexdigest() + '.jpg'
                    df = df.append(data, ignore_index=True)
                    if len(df) >= 1000:
                        try:
                            store_to_database(df)
                            df = pd.DataFrame(columns=['brand', 'model', 'type', 'year', 'image_url'])
                        except:
                            pass

                except:
                    pass

            driver.quit()
        store_to_database(df)

    @staticmethod
    def get_queries(brand):
        queries = []
        try:
            params = config()
            conn = psycopg2.connect(**params)
            cur = conn.cursor()
            query = "select concat(brand,' + ',model,' + ',type, ' + ', year) as query from label where brand='{}'".format(
                brand)
            cur.execute(query)
            queries = cur.fetchall()
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            logging.warning(error)
        return [tup[0] for tup in queries]

    def crawl(self):
        try:
            params = config()
            conn = psycopg2.connect(**params)
            cur = conn.cursor()
            cur.execute('select brand from brand')
            brands = cur.fetchall()
            brands = [tup[0] for tup in brands]
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            logging.warning(error)

        my_threads = []

        for brand in brands:
            t = threading.Thread(target=self.get_images, args=(self.get_queries(brand),))
            t.start()
            my_threads.append(t)

        for t in my_threads:
            t.join()


google_image = GoogleImage()
google_image.crawl()
