from flask import Flask, flash, request, redirect, url_for, render_template
import os
from werkzeug.utils import secure_filename
from flask import jsonify
from tensorflow.keras.applications.vgg19 import preprocess_input
import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd

app = Flask(__name__)

UPLOAD_FOLDER = '/home/fancol/Downloads/'

app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def get_image(img_path):
    try:
        image = tf.io.read_file(img_path)
        image = tf.io.decode_jpeg(image, channels=3)
        image = tf.image.resize(image, [224, 224])
        image = preprocess_input(image)
        image = np.expand_dims(image, axis=0)
        return image
    except:
        return None


def image(filename):
    img_path = app.config['UPLOAD_FOLDER'] + filename
    if get_image(img_path) is not None:
        image = get_image(img_path)

        spe_model = keras.models.load_model('weight/specialized.h5')
        trek_model = keras.models.load_model('weight/trek.h5')
        giant_model = keras.models.load_model('weight/giant.h5')
        can_model = keras.models.load_model('weight/cannondale.h5')
        type_model = keras.models.load_model('weight/type.h5')
        brand_model = keras.models.load_model('weight/brand.h5')

        spe_pred = spe_model.predict(image)
        trek_pred = trek_model.predict(image)
        giant_pred = giant_model.predict(image)
        can_pred = can_model.predict(image)
        type_pred = type_model.predict(image)
        brand_pred = brand_model.predict(image)

        data = pd.read_csv('./database_label/model.csv')
        spe_classes = data[data['brand_id'] == 1]['model'].tolist()
        trek_classes = data[data['brand_id'] == 2]['model'].tolist()
        can_classes = data[data['brand_id'] == 3]['model'].tolist()
        giant_classes = data[data['brand_id'] == 4]['model'].tolist()

        data = pd.read_csv('./database_label/type.csv')
        type_classes = data['type'].tolist()

        if np.argmax(brand_pred) == 0:
            return jsonify(
                model=spe_classes[np.argmax(spe_pred)],
                brand="Specialized",
                accuracy=str(np.max(spe_pred)),
                type=str(type_classes[np.argmax(type_pred)]),
            )
        elif np.argmax(brand_pred) == 1:
            return jsonify(
                model=trek_classes[np.argmax(trek_pred)],
                brand="Trek",
                accuracy=str(np.max(trek_pred)),
                type=str(type_classes[np.argmax(type_pred)])
            )

        elif np.argmax(brand_pred) == 2:
            return jsonify(
                model=can_classes[np.argmax(can_pred)],
                brand="Cannondale",
                accuracy=str(np.max(can_pred)),
                type=str(type_classes[np.argmax(type_pred)])
            )
        else:
            return jsonify(
                model=giant_classes[np.argmax(giant_pred)],
                brand="Giant",
                accuracy=str(np.max(giant_pred)),
                type=str(type_classes[np.argmax(type_pred)])
            )
    else:
        return "url not found"


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def upload_image():
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No image selected for uploading')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        return image(filename)
    else:
        flash('Allowed image types are - png, jpg, jpeg, gif')
        return redirect(request.url)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)