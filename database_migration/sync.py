import psycopg2
import logging
import pathlib
from config import config
from os import listdir

def sync(brand, table, dir_path):
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("select image_url from {} where brand ='{}'".format(table, brand))
        image_urls = cur.fetchall()
        image_urls = [tup[0] for tup in image_urls]
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.warning(error)

    images = listdir(str(pathlib.Path(__file__).resolve().parents[1]) + "/{}/".format(dir_path))
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        for image_url in image_urls:
            if image_url.split("/")[1] not in images:
                cur.execute("delete from {} where image_url = '{}'".format(table, image_url))
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.warning(error)

sync("Specialized", "image", "data")
sync("Trek", "image", "data")
sync("Cannondale", "image", "data")
sync("Giant", "image", "data")


