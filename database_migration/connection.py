import psycopg2
import logging
import pandas as pd
from os.path import dirname, abspath
from config import config
from config import config_url
from sqlalchemy import create_engine


def initialize():
    """ create tables in the PostgreSQL database"""
    create_cmds = (
        """
        CREATE TABLE brand (
            id SERIAL PRIMARY KEY,
            brand VARCHAR(255) NOT NULL
        )
        """,
        """ CREATE TABLE type (
                id SERIAL PRIMARY KEY,
                type VARCHAR(255) NOT NULL
                )
        """,
        """
        CREATE TABLE year (
                year INTEGER PRIMARY KEY
        )
        """,
        """
        CREATE TABLE model (
                id SERIAL PRIMARY KEY,
                model VARCHAR (255) NOT NULL,
                brand_id INTEGER NOT NULL
        )
        """,
        """
        CREATE TABLE image (
                id SERIAL PRIMARY KEY,
                brand VARCHAR(255) NOT NULL,
                model VARCHAR(255) NOT NULL,
                type VARCHAR(255) NOT NULL,
                year INTEGER NOT NULL,
                image_url VARCHAR (255) NOT NULL
        )
        """,
        """
        CREATE TABLE label (
                id SERIAL PRIMARY KEY,
                brand VARCHAR(255) NOT NULL,
                model VARCHAR(255) NOT NULL,
                type VARCHAR(255) NOT NULL,
                year INTEGER NOT NULL
        )
        """
        )

    # def insert_one_cmds(table, columns, file_path):
    #     a = []
    #     insert_str = """INSERT INTO {}({})
    #                     VALUES ({})
    #                     """
    #     with open(file_path, "r") as f:
    #         for line in f:
    #             values = line.rstrip('\n').replace("'", "''").split(", ")
    #             values = ["'{}'".format(i) for i in values]
    #             values = ", ".join(values)
    #             a.append(insert_str.format(table, columns, values))
    #     return a

    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        path = dirname(dirname(abspath(__file__)))+"/database_config/"
        for command in create_cmds:
            cur.execute(command)
        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.warning(error)

    engine = create_engine(config_url())
    data = pd.read_csv(path+'label.csv')
    data.to_sql('label', con=engine, index=False, if_exists='append', chunksize=1000)
    data = pd.read_csv(path + 'brand.csv')
    data.to_sql('brand', con=engine, index=False, if_exists='append', chunksize=1000)
    data = pd.read_csv(path + 'type.csv')
    data.to_sql('type', con=engine, index=False, if_exists='append', chunksize=1000)
    data = pd.read_csv(path + 'model.csv')
    data.to_sql('model', con=engine, index=False, if_exists='append', chunksize=1000)
    data = pd.read_csv(path + 'year.csv')
    data.to_sql('year', con=engine, index=False, if_exists='append', chunksize=1000)

if __name__ == '__main__':
    initialize()
