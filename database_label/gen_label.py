import pandas as pd
import numpy as np

data = pd.read_csv(r'sample.csv')
sample_df = pd.DataFrame(data, columns=['brand', 'model', 'type', 'year'])

data = pd.read_csv(r'brand.csv')
brand_df = pd.DataFrame(data, columns=['brand', 'id'])
brand_df = brand_df.set_index('id')

data = pd.read_csv(r'model.csv')
model_df = pd.DataFrame(data, columns=['id', 'brand_id', 'model'])
model_df = model_df.set_index('id')

label = model_df.join(brand_df, on='brand_id')


def get_elements(brand, model):
    df = sample_df[sample_df['brand'] == brand]
    return df[df['model'] == model]

label_df = pd.DataFrame()
for index, row in label.iterrows():
    label_df = label_df.append(get_elements(row['brand'], row['model']), ignore_index=True)
label_df['id'] = np.arange(1, len(label_df)+1)
label_df.to_csv('label.csv',columns=['id', 'brand', 'model', 'type', 'year'],index=False)
