absl-py==0.13.0
argon2-cffi @ file:///tmp/build/80754af9/argon2-cffi_1613037097816/work
astunparse==1.6.3
async-generator @ file:///home/ktietz/src/ci/async_generator_1611927993394/work
attrs @ file:///tmp/build/80754af9/attrs_1620827162558/work
backcall @ file:///home/ktietz/src/ci/backcall_1611930011877/work
beautifulsoup4==4.9.3
bleach @ file:///tmp/build/80754af9/bleach_1628110601003/work
cachetools==4.2.2
certifi==2021.5.30
cffi @ file:///tmp/build/80754af9/cffi_1625807838443/work
charset-normalizer==2.0.4
clang==5.0
click==8.0.1
colorama==0.4.4
configparser==5.0.2
crayons==0.4.0
decorator @ file:///tmp/build/80754af9/decorator_1621259047763/work
defusedxml @ file:///tmp/build/80754af9/defusedxml_1615228127516/work
entrypoints==0.3
Flask==2.0.1
flatbuffers==1.12
gast==0.4.0
google-auth==1.35.0
google-auth-oauthlib==0.4.5
google-pasta==0.2.0
greenlet==1.1.1
grpcio==1.39.0
h5py==3.1.0
idna==3.2
importlib-metadata @ file:///tmp/build/80754af9/importlib-metadata_1617874469820/work
ipykernel @ file:///tmp/build/80754af9/ipykernel_1596207638929/work/dist/ipykernel-5.3.4-py3-none-any.whl
ipython @ file:///tmp/build/80754af9/ipython_1628243924020/work
ipython-genutils @ file:///tmp/build/80754af9/ipython_genutils_1606773439826/work
ipywidgets @ file:///tmp/build/80754af9/ipywidgets_1610481889018/work
itsdangerous==2.0.1
jedi @ file:///tmp/build/80754af9/jedi_1611333125342/work
Jinja2 @ file:///tmp/build/80754af9/jinja2_1624781299557/work
jsonschema @ file:///tmp/build/80754af9/jsonschema_1602607155483/work
jupyter==1.0.0
jupyter-client @ file:///tmp/build/80754af9/jupyter_client_1616770841739/work
jupyter-console @ file:///tmp/build/80754af9/jupyter_console_1616615302928/work
jupyter-core @ file:///tmp/build/80754af9/jupyter_core_1612213311222/work
jupyterlab-pygments @ file:///tmp/build/80754af9/jupyterlab_pygments_1601490720602/work
jupyterlab-widgets @ file:///tmp/build/80754af9/jupyterlab_widgets_1609884341231/work
keras==2.6.0
Keras-Preprocessing==1.1.2
Markdown==3.3.4
MarkupSafe @ file:///tmp/build/80754af9/markupsafe_1621528148836/work
matplotlib-inline @ file:///tmp/build/80754af9/matplotlib-inline_1628242447089/work
mistune==0.8.4
nbclient @ file:///tmp/build/80754af9/nbclient_1614364831625/work
nbconvert @ file:///tmp/build/80754af9/nbconvert_1624479060632/work
nbformat @ file:///tmp/build/80754af9/nbformat_1617383369282/work
nest-asyncio @ file:///tmp/build/80754af9/nest-asyncio_1613680548246/work
notebook @ file:///tmp/build/80754af9/notebook_1629205607169/work
numpy==1.19.5
oauthlib==3.1.1
opt-einsum==3.3.0
packaging @ file:///tmp/build/80754af9/packaging_1625611678980/work
pandas==1.3.1
pandocfilters @ file:///tmp/build/80754af9/pandocfilters_1605120460739/work
parso @ file:///tmp/build/80754af9/parso_1617223946239/work
pexpect @ file:///tmp/build/80754af9/pexpect_1605563209008/work
pickleshare @ file:///tmp/build/80754af9/pickleshare_1606932040724/work
Pillow==8.3.1
prometheus-client @ file:///tmp/build/80754af9/prometheus_client_1623189609245/work
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1616415428029/work
protobuf==3.17.3
psycopg2-binary==2.9.1
ptyprocess @ file:///tmp/build/80754af9/ptyprocess_1609355006118/work/dist/ptyprocess-0.7.0-py2.py3-none-any.whl
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser @ file:///tmp/build/80754af9/pycparser_1594388511720/work
Pygments @ file:///tmp/build/80754af9/pygments_1629234116488/work
pyparsing @ file:///home/linux1/recipes/ci/pyparsing_1610983426697/work
pyrsistent @ file:///tmp/build/80754af9/pyrsistent_1600141720057/work
PyScrappy==0.0.9
python-dateutil @ file:///tmp/build/80754af9/python-dateutil_1626374649649/work
pytz==2021.1
pyzmq @ file:///tmp/build/80754af9/pyzmq_1628276010766/work
qtconsole @ file:///tmp/build/80754af9/qtconsole_1623278325812/work
QtPy==1.9.0
requests==2.26.0
requests-oauthlib==1.3.0
rsa==4.7.2
selenium==3.141.0
Send2Trash @ file:///tmp/build/80754af9/send2trash_1607525499227/work
sip==4.19.13
six @ file:///tmp/build/80754af9/six_1623709665295/work
soupsieve==2.2.1
SQLAlchemy==1.4.22
tensorboard==2.6.0
tensorboard-data-server==0.6.1
tensorboard-plugin-wit==1.8.0
tensorflow==2.6.0
tensorflow-estimator==2.6.0
termcolor==1.1.0
terminado==0.9.4
testpath @ file:///tmp/build/80754af9/testpath_1624638946665/work
tornado @ file:///tmp/build/80754af9/tornado_1606942300299/work
traitlets @ file:///home/ktietz/src/ci/traitlets_1611929699868/work
typing-extensions==3.7.4.3
urllib3==1.26.6
wcwidth @ file:///tmp/build/80754af9/wcwidth_1593447189090/work
webdriver-manager==3.4.2
webencodings==0.5.1
Werkzeug==2.0.1
widgetsnbextension==3.5.1
wrapt==1.12.1
zipp @ file:///tmp/build/80754af9/zipp_1625570634446/work
